public class Card {
	private String suit;
	private String value;
	
	//constructor
	public Card(String suit, String value) {
		this.suit = suit;
		this.value = value;
	}
	
	//get methods
	public String getSuit() {
		return this.suit;
	}
	
	public String getValue() {
		value = this.value;
		
		//change displayed value without actually changing the value field
		if (this.value.equals("1")) {
			value = "Ace";
		} else if (this.value.equals("11")) {
			value = "Jack";
		} else if (this.value.equals("12")) {
			value = "Queen";
		} else if (this.value.equals("13")) {
			value = "King";
		}
		
		return value;
	}
	
	//toString
	public String toString() {
		value = this.value;
		
		//change displayed value without actually changing the value field
		if (this.value.equals("1")) {
			value = "Ace";
		} else if (this.value.equals("11")) {
			value = "Jack";
		} else if (this.value.equals("12")) {
			value = "Queen";
		} else if (this.value.equals("13")) {
			value = "King";
		}
		
		return value + " Of " + this.suit;
	}
	
	//CUSTOM METHODS
	//returns the score of a card using its value and suit
	public double calculateScore() {
		double score = Double.parseDouble(this.value);
		
		if (this.suit.equals("Hearts")) {
			score += 0.4;
		} else if (this.suit.equals("Diamonds")) {
			score += 0.3;
		} else if (this.suit.equals("Clubs")) {
			score += 0.2;
		} else {
			score += 0.1;
		}
		
		return score;
	}
}